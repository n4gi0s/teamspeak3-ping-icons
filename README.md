![Screenshot](https://gitlab.com/cyberdos/teamspeak3-ping-icons/-/raw/master/screenshot.png)


## Connection icons
The connection quality of all clients is described with the following icons:

|Packet loss|Verybad connection|Bad connection:|Good connection|
|--|--|--|--|
|![packet_loss](https://gitlab.com/cyberdos/teamspeak3-ping-icons/-/raw/master/icons/packet_loss.png)|![verybad](https://gitlab.com/cyberdos/teamspeak3-ping-icons/-/raw/master/icons/verybad.png)|![bad](https://gitlab.com/cyberdos/teamspeak3-ping-icons/-/raw/master/icons/bad.png)|![good](https://gitlab.com/cyberdos/teamspeak3-ping-icons/-/raw/master/icons/good.png)|

# Installation

## Download files

    git clone https://gitlab.com/cyberdos/teamspeak3-ping-icons.git
    cd teamspeak3-ping-icons

## Configuration

1. Upload .png files from icons folder to teamspeak3 server (using a query tool)
2. Copy config-sample.json to config.json
3. Replace the icon ids with your uploaded file ids (optional)
4. Create teamspeak3 query user (optional) 	Teamspeak client -> Extras -> ServerQuery Login
5. Replace the credentials in the config file with your own ones
6. Add serverport: YOURPORT to the teamspeak section (optional)

## 1. Legacy
This tools requires Node.JS (download: [nodejs.org](https://nodejs.org/de/download/))

    cd src
    npm install
    node main.js
    

## 2. Docker

    docker build -t cyberdos/teamspeak3-ping-icons .
    docker run --restart unless-stopped -d cyberdos/teamspeak3-ping-icons

Icons by @Acie