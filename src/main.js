const {
    TeamSpeak
} = require("ts3-nodejs-library")
var ping = require('ping');
const fs = require('fs')

//check if config exists
let configPath = "./config.json";
if (!fs.existsSync(configPath)) return console.error("*CONFIG MISSING*", "rename config-sample.json -> config.json");
const config = require(configPath);

(async () => {
    let teamspeak = await TeamSpeak.connect(config.teamspeak);
    while (true) {
        let clients = await teamspeak.clientList({
            client_type: 0
        });
        await Promise.all(clients.map(async client => {
            let clientInfo = await client.getInfo();
            let pingRes = await ping.promise.probe(clientInfo.connection_client_ip, config.ping);
            let avg = Number(pingRes.avg);
            if (isNaN(avg)) avg = -1;
            let packetLoss = Number(pingRes.packetLoss);
            if (isNaN(packetLoss)) packetLoss = 100;
            console.log(client.nickname, avg, packetLoss); //debug
            if (config.setDescription) client.edit({
                client_description: `Ping: ${avg.toFixed(0)}ms, Packetloss: ${packetLoss.toFixed(0)}%`
            });
            if (!pingRes.alive) return client.delPerm("i_icon_id"); //ignore blocked ping
            if (packetLoss > 0) return client.addPerm("i_icon_id", config.icons.packet_loss); //packet loss
            if (avg >= config.ping_levels.verybad) return client.addPerm("i_icon_id", config.icons.verybad); //red
            if (avg >= config.ping_levels.bad) return client.addPerm("i_icon_id", config.icons.bad); //yellow
            client.addPerm("i_icon_id", config.icons.good); //green
        }));
    }
})();